# Wych

A dependently typed programming language inspired by Elm and Agda.
This project is currently early in development.
The current plan is to compile to Javascript.

## Roadmap

- Type system
  - Dependent types (complete)
  - Type inference (in progress)
  - GADTs
  - Row polymorpism
    - Lenses
  - "type classes"
- Language semantics
  - Termination checking
  - IO model (similar to Elm's?)
- Runtime
  - Decide Target
  - VDOM
- Parsing
- Standard library
