module Main where


import Evaluate ()
import Nat ()
import PrettyPrint ()
import Term ()
import TypeCheck ()
import TypeCheck2 ()


main :: IO ()
main = do
  putStrLn "hello world"
