module Nat where


data Nat
  = Zero
  | Succ Nat
  deriving
    ( Eq
    , Ord
    , Show
    )


join :: Nat -> Nat -> Nat
join n m =
  go n m
  where
    go :: Nat -> Nat -> Nat
    go n' m' =
      case
        n'
      of
        Zero ->
          m

        Succ n'' ->
          case
            m'
          of
            Zero ->
              n

            Succ m'' ->
              go n'' m''
