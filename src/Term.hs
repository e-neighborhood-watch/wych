module Term where


import Nat


import Data.Void


type Term = PartialTerm Void


data PartialTerm a
  = PartialTerm a ::: PartialTerm a
  | TypeOfTypes Nat
  | Pi (PartialTerm a) (PartialTerm a)
  | Var Nat
  | App (PartialTerm a) (PartialTerm a)
  | Abs (PartialTerm a)
  | Rec (PartialTerm a)
  | Hole a
  deriving
    ( Show
    , Eq
    )


instance Functor PartialTerm where
  fmap f term =
    case
      term
    of
      t1 ::: t2 ->
        fmap f t1 ::: fmap f t2

      TypeOfTypes n ->
        TypeOfTypes n

      Pi t1 t2 ->
        Pi (fmap f t1) (fmap f t2)

      Var n ->
        Var n

      App t1 t2 ->
        App (fmap f t1) (fmap f t2)

      Abs t ->
        Abs $ fmap f t

      Rec t ->
        Rec $ fmap f t

      Hole a ->
        Hole $ f a


reIndex :: PartialTerm a -> PartialTerm a
reIndex =
  go Zero
  where
    go :: Nat -> PartialTerm a -> PartialTerm a
    go depth term =
      case
        term
      of
        Abs term' ->
          Abs $ go (Succ depth) term'

        Pi term1 term2 ->
          Pi (go depth term1) (go (Succ depth) term2)

        term1 ::: term2 ->
          go depth term1 ::: go depth term2

        TypeOfTypes level ->
          TypeOfTypes level

        App term1 term2 ->
          App (go depth term1) (go depth term2)

        Var nat ->
          case
            compare nat depth
          of
            LT ->
              Var nat
            _ ->
              Var $ Succ nat

        Rec term' ->
          Rec (go (Succ depth) term')

        Hole a ->
          Hole a


(-->) :: Term -> Term -> Term
t1 --> t2 =
  Pi t1 $ reIndex t2
infixr 1 -->
