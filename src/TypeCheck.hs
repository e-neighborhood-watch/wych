module TypeCheck where


import Evaluate
import Nat
import PrettyPrint
import Term


import Data.Void


type Context =
  [ PartialValue Void ]


reIndexValue :: Value -> Value
reIndexValue =
  go Zero
  where
    go :: Nat -> Value -> Value
    go depth value =
      case
        value
      of
        NeutralTerm neutral ->
          NeutralTerm $ go' depth neutral

        VTypeOfTypes level ->
          VTypeOfTypes level

        VPi value1 value2 ->
          VPi (go depth value1) (go (Succ depth) value2)

        VAbs term' ->
          VAbs $ go (Succ depth) term'

    go' :: Nat -> Neutral -> Neutral
    go' depth neutral =
      case
        neutral
      of
        NVar nat ->
          case
            compare nat depth
          of
            LT ->
              NVar nat

            _ ->
              NVar $ Succ nat

        NApp neutral' value ->
          NApp (go' depth neutral') (go depth value)

        NHole a ->
          absurd a


lookupVarType :: Nat -> Context -> Either String Value
lookupVarType _ [] =
  Left "Type of var is undefined"
lookupVarType Zero (varType : _) =
  Right varType
lookupVarType (Succ depth) (_ : context) =
  lookupVarType depth context


getTypeLevel :: Context -> Term -> Either String Nat
getTypeLevel context term =
  do
    inferredType <- inferType context term
    case
      inferredType
     of
      VTypeOfTypes n ->
        pure n

      notTypeOfTypes ->
        Left $ "Expected type instead of value of type " ++ prettyPrint (valueToTerm notTypeOfTypes)


inferType :: Context -> Term -> Either String Value
inferType context term =
  case
    term
  of
    term' ::: term'Type ->
      do
        _ <- getTypeLevel context term'Type
        let
          eval'dType :: Value
          eval'dType =
            evaluate term'Type
        checkType context term' eval'dType
        pure eval'dType

    TypeOfTypes level->
      pure $ VTypeOfTypes $ Succ level

    Pi argType resultType ->
      do
        n <- getTypeLevel context argType
        m <- getTypeLevel (reIndexValue <$> evaluate argType : context) resultType
        pure $ VTypeOfTypes $ join n m

    Var ix ->
      lookupVarType ix context

    App func arg ->
      do
        funcType <- inferType context func
        case
          funcType
         of
          VPi argType resultType ->
            do
              checkType context arg argType
              pure $ evaluate $ substitute arg resultType

          _ ->
            Left "Cannot apply term with non-function type"

    Abs _ ->
      Left "Lambda abstraction has ambiguous type"

    Rec _ ->
      Left "Recursive abstraction has ambiguous type"

    Hole a ->
      absurd a


checkType :: Context -> Term -> Value -> Either String ()
checkType context term termType =
  case
    term
  of
    Abs term' ->
      case
        termType
      of
        VPi term1 term2 ->
          getTypeLevel context (valueToTerm term1)
           >> checkType (reIndexValue <$> term1 : context) term' term2

        notPi ->
          Left $ "Lambda abstractions must have a function type. Cannot type " ++ prettyPrint term ++ " : " ++ prettyPrint (valueToTerm notPi)

    Rec term' ->
      checkType (reIndexValue <$> termType : context) term' (reIndexValue termType)

    _ ->
      do
        inferredType <- inferType context term
        if
          termType == inferredType
         then
          pure ()
         else
          Left $ "Conflicting types found when checking types. Inferred " ++ prettyPrint term ++ " : " ++ prettyPrint (valueToTerm inferredType) ++ " =/= " ++ prettyPrint (valueToTerm termType)
