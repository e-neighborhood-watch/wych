module TypeCheck2 where


import Evaluate
import Term


import Control.Applicative

import Data.Function (on)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Void


type HoleId =
  Integer


succHole :: HoleId -> HoleId
succHole =
  (1+)


type ValueWithHoles =
  PartialValue HoleId


type NeutralWithHoles =
  PartialNeutral HoleId


type TermWithHoles =
  PartialTerm HoleId


data Context =
  Context
    { nextHole :: HoleId
    , varContext :: [ValueWithHoles]
    }


data TypeCheckError
  = TypeMismatch
  | TypeCircularity
  | TypeAmbiguity


type Substitutions =
  Map HoleId ValueWithHoles


type TypeCheck =
  Either TypeCheckError


applySubstitutions :: Substitutions -> ValueWithHoles -> ValueWithHoles
applySubstitutions subs val =
  case
    val
  of
    NeutralTerm n ->
      evaluate $ applySubstitutionsNeutral subs n

    VTypeOfTypes l ->
      VTypeOfTypes l

    VPi v1 v2 ->
      VPi (applySubstitutions subs v1) (applySubstitutions subs v2)

    VAbs v' ->
      VAbs $ applySubstitutions subs v'
  where
    applySubstitutionsNeutral :: Substitutions -> NeutralWithHoles -> TermWithHoles
    applySubstitutionsNeutral subsn n =
      case
        n
      of
        NVar v ->
          Var v

        NApp n' v ->
          App (applySubstitutionsNeutral subsn n') $ valueToTerm $ applySubstitutions subsn v

        NHole h ->
          case
            Map.lookup h subsn
          of
            Nothing ->
              Hole h

            Just sub ->
              valueToTerm $ applySubstitutions subs sub -- TODO do we need apply subs to this?


occursIn :: HoleId -> ValueWithHoles -> Bool
occursIn hid val =
  case
    val
  of
    NeutralTerm n ->
      hid `occursInNeutral` n

    VTypeOfTypes _ ->
      False

    VPi v1 v2 ->
      (hid `occursIn` v1) || (hid `occursIn` v2)

    VAbs v' ->
      hid `occursIn` v'
  where
    occursInNeutral :: HoleId -> NeutralWithHoles -> Bool
    occursInNeutral idn neu =
      case
        neu
      of
        NVar _ ->
          False

        NApp n v ->
          (idn `occursInNeutral` n) || (idn `occursIn` v)

        NHole idq ->
          idn == idq


unify :: ValueWithHoles -> ValueWithHoles -> TypeCheck Substitutions
unify v1 v2 =
  case
    (v1, v2)
  of
    (NeutralTerm (NHole a), v) ->
      if
        a `occursIn` v
      then
        Left TypeCircularity
      else
        pure $ Map.singleton a v

    (v, NeutralTerm (NHole a)) ->
      if
        a `occursIn` v
      then
        Left TypeCircularity
      else
        pure $ Map.singleton a v

    (NeutralTerm n1, NeutralTerm n2) ->
      unifyNeutral n1 n2

    (VTypeOfTypes l1, VTypeOfTypes l2) ->
      if
        l1 == l2
      then
        pure Map.empty
      else
        Left TypeMismatch

    (VPi v'1 v'2, VPi v'3 v'4) ->
      do
        sub1 <- unify v'1 v'3
        Map.union sub1 <$> unify (applySubstitutions sub1 v'2) (applySubstitutions sub1 v'4)

    (VAbs v'1, VAbs v'2) ->
      unify v'1 v'2

    (_, _) ->
      Left TypeMismatch
  where
    unifyNeutral :: NeutralWithHoles -> NeutralWithHoles -> TypeCheck Substitutions
    unifyNeutral n1 n2 =
      case
        (n1, n2)
      of
        (NHole a, n) ->
          if
            a `occursIn` NeutralTerm n
          then
            Left TypeCircularity
          else
            pure $ Map.singleton a $ NeutralTerm n

        (n, NHole a) ->
          if
            a `occursIn` NeutralTerm n
          then
            Left TypeCircularity
          else
            pure $ Map.singleton a $ NeutralTerm n

        (NVar p1, NVar p2) ->
          if
            p1 == p2
          then
            pure Map.empty
          else
            Left TypeMismatch

        (NApp n'1 v'1, NApp n'2 v'2) ->
          do
            sub1 <- unifyNeutral n'1 n'2
            Map.union sub1 <$> unify (applySubstitutions sub1 v'1) (applySubstitutions sub1 v'2)

        (_, _) ->
          Left TypeMismatch


typeCheck :: Term -> TypeCheck Value
typeCheck =
  (>>= noHoles . snd)
    . go
      Context
        { nextHole =
          0
        , varContext =
          []
        }
    . vacuous
  where
    go :: Context -> TermWithHoles -> TypeCheck ( Substitutions, ValueWithHoles )
    go = undefined

    noHoles :: ValueWithHoles -> TypeCheck Value
    noHoles value =
      case
        value
      of
        NeutralTerm n ->
          NeutralTerm <$> noHolesN n

        VTypeOfTypes n ->
          pure $ VTypeOfTypes n

        VPi v1 v2 ->
          (liftA2 VPi `on` noHoles) v1 v2

        VAbs v ->
          VAbs <$> noHoles v

    noHolesN :: NeutralWithHoles -> TypeCheck Neutral
    noHolesN neutral =
      case
        neutral
      of
        NVar n ->
          pure $ NVar n

        NApp n v ->
          liftA2 NApp (noHolesN n) (noHoles v)

        NHole _ ->
          Left TypeAmbiguity
