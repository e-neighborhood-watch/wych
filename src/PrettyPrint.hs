module PrettyPrint where


import Evaluate
import Nat
import Term


class PrettyPrint a where
  prettyPrint :: a -> String


instance Show a => PrettyPrint (PartialValue a) where
  prettyPrint =
    show


instance PrettyPrint Nat where
  prettyPrint =
    show . go 0
    where
      go :: Integer -> Nat -> Integer
      go n Zero =
        n
      go n (Succ m) =
        go (succ n) m


instance Show a => PrettyPrint (PartialTerm a) where
  prettyPrint =
    go 0 0 []
    where
      dependent :: Nat -> PartialTerm a -> Bool
      dependent n term =
        case
          term
        of
          TypeOfTypes _ ->
            False

          Pi term1 term2 ->
            dependent n term1 || dependent (Succ n) term2

          Abs term' ->
            dependent (Succ n) term'

          Var n' ->
            n == n'

          App term1 term2 ->
            dependent n term1 || dependent n term2

          term1 ::: term2 ->
            dependent n term1 || dependent n term2

          Rec term' ->
            dependent (Succ n) term'

          Hole _ ->
            False

      lookupName :: Nat -> [String] -> String
      lookupName nat names =
        case
          names
        of
          [] ->
            "?" ++ prettyPrint nat

          (n : ns) ->
            case
              nat
            of
              Zero ->
                n

              Succ nat' ->
                lookupName nat' ns

      goAbs :: Show a => Integer -> Integer -> [String] -> PartialTerm a -> String
      goAbs tn xn names term =
        let
          (newName, inc) =
            if
              dependent Zero term
            then
              ("x" ++ show xn, 1)
            else
              ("_", 0)
        in
          newName
            ++ " "
            ++
              case
                term
              of
                Abs term' ->
                  goAbs tn (xn+inc) (newName:names) term'

                notAbs ->
                  ". " ++ go tn (xn+inc) (newName:names) notAbs

      goPi :: Show a => Integer -> Integer -> [String] -> PartialTerm a -> PartialTerm a -> String
      goPi tn xn names argType resType =
        let
          newName :: String
          newName =
            "t" ++ show tn
        in
          "(" ++ newName ++ " : " ++ go tn xn names argType ++ ") " ++
          case
            resType
          of
            Pi argType' resType'
              | dependent Zero resType' ->
              goPi (tn+1) xn (newName:names) argType' resType'

            notDepPi ->
              ". " ++ go (tn+1) xn (newName:names) notDepPi

      goFunc :: Show a => Integer -> Integer -> [String] -> PartialTerm a -> PartialTerm a -> String
      goFunc tn xn names argType resType =
        go tn xn names argType ++ " -> " ++
          case
            resType
          of
            Pi argType' resType'
              | not $ dependent Zero resType' ->
              goFunc tn xn ("_":names) argType' resType'

            notFunc ->
              go tn xn ("_":names) notFunc

      goApp :: Show a => Integer -> Integer -> [String] -> PartialTerm a -> PartialTerm a -> String
      goApp tn xn names func arg =
        (++ (" " ++ go tn xn names arg)) $
        case
          func
        of
          App func' arg' ->
            goApp tn xn names func' arg'

          notApp ->
            go tn xn names notApp

      go :: Show a => Integer -> Integer -> [String] -> PartialTerm a -> String
      go tn xn names term =
        case
          term
        of
          TypeOfTypes n ->
            prettyPrint n ++ "*"

          Pi term1 term2 ->
            if
              dependent Zero term2
            then
              "(forall " ++ goPi tn xn names term1 term2 ++ ")"
            else
              "(" ++ goFunc tn xn names term1 term2 ++ ")"

          Abs term' ->
            "(\\ " ++ goAbs tn xn names term' ++ ")"

          Var n ->
            lookupName n names

          App term1 term2 ->
            "(" ++ goApp tn xn names term1 term2 ++ ")"

          term1 ::: term2 ->
            "(" ++ go tn xn names term1 ++ " : " ++ go tn xn names term2 ++ ")"

          Rec term' ->
            let
              recName :: String
              recName =
                "x" ++ show xn
            in
              "( let " ++ recName ++ " = " ++ go tn (xn+1) (recName:names) term' ++ " in " ++ recName ++ ")"

          Hole a ->
            "{" ++ show a ++ "}"
