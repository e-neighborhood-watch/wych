module Evaluate where


import Nat
import Term


import Data.Void


data PartialValue a
  = NeutralTerm (PartialNeutral a)
  | VTypeOfTypes Nat
  | VPi (PartialValue a) (PartialValue a)
  | VAbs (PartialValue a)
  deriving
    ( Show
    , Eq
    )


instance Functor PartialValue where
  fmap f pval =
    case
      pval
    of
      NeutralTerm pneutral' ->
        NeutralTerm $ fmap f pneutral'

      VTypeOfTypes nat ->
        VTypeOfTypes nat

      VPi pval1 pval2 ->
        VPi
          (fmap f pval1)
          (fmap f pval2)

      VAbs pval' ->
        VAbs $ fmap f pval'


type Value =
  PartialValue Void


data PartialNeutral a
  = NVar Nat
  | NApp (PartialNeutral a) (PartialValue a)
  | NHole a
  deriving
    ( Show
    , Eq
    )


instance Functor PartialNeutral where
  fmap f pneutral =
    case
      pneutral
    of
      NVar nat ->
        NVar nat

      NApp pneutral' pval ->
        NApp
          (fmap f pneutral')
          (fmap f pval)

      NHole a ->
        NHole $ f a


type Neutral =
  PartialNeutral Void


evaluate :: PartialTerm a -> PartialValue a
evaluate term =
  case
    term
  of
    term' ::: _ ->
      evaluate term'

    TypeOfTypes level ->
      VTypeOfTypes level

    Pi term1 term2 ->
      VPi (evaluate term1) (evaluate term2)

    Var n ->
      NeutralTerm $ NVar n

    Abs term' ->
      VAbs $ evaluate term'

    App term1 term2 ->
      case
        evaluate term1
      of
        VAbs value ->
          evaluate $ substitute term2 value

        NeutralTerm neutral ->
          NeutralTerm $ NApp neutral $ evaluate term2

        _ ->
          error "type checker should have caught this"

    Hole a ->
      NeutralTerm $ NHole a

    Rec _ ->
      error "TODO not yet implemented"


substitute :: PartialTerm a -> PartialValue a -> PartialTerm a
substitute =
  go Zero
  where
    go :: Nat -> PartialTerm a -> PartialValue a -> PartialTerm a
    go depth term value =
      case
        value
      of
        VAbs value' ->
          Abs (go (Succ depth) (reIndex term) value')

        VPi value1 value2 ->
          Pi (go depth term value1) (go (Succ depth) (reIndex term) value2)

        VTypeOfTypes level ->
          TypeOfTypes level

        NeutralTerm (NVar nat') ->
          case
            compare nat' depth
          of
            LT ->
              Var nat'
            GT ->
              case
                nat'
              of
                Zero ->
                  Var Zero -- this should never happen
                Succ n ->
                  Var n
            EQ ->
              term

        NeutralTerm (NApp v1 v2) ->
          App (go depth term $ NeutralTerm v1) (go depth term v2)

        NeutralTerm (NHole a) ->
          Hole a


valueToTerm :: PartialValue a -> PartialTerm a
valueToTerm value =
  case
    value
  of
    VTypeOfTypes level ->
      TypeOfTypes level

    VPi value1 value2 ->
      Pi (valueToTerm value1) (valueToTerm value2)

    VAbs value' ->
      Abs (valueToTerm value')

    NeutralTerm neutral ->
      neutralToTerm neutral
  where
    neutralToTerm :: PartialNeutral a -> PartialTerm a
    neutralToTerm neutral =
      case
        neutral
      of
        NVar ix ->
          Var ix

        NApp neutral' value' ->
          App (neutralToTerm neutral') (valueToTerm value')

        NHole a ->
          Hole a
